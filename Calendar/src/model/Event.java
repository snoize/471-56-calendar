package model;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import controlview.DateAndTimeFormat;


public class Event extends AbstractTimeDuration {

	private String description;
//	private String location;
//	private String name;
	private String type = "Event"; 
//	private int day;
//	private int month;
//	private int sumEstTime;
	
	private String days = "";
	private String months = "";
	
//	private Date to;
//	private Date from;
	
	private int estTime;

	Account acc = new Account();
	Date dateFrom, dateTo;
	
	
	public Event() {
		
	}
	
	public Event(String descript, String from, String to/*, int estTime, boolean done*/) throws ParseException {
//		this.from = from;
//		this.to = to;
		setType(type);

		dateFrom = DateAndTimeFormat.dateTimeFormat.parse(from);
		dateTo = DateAndTimeFormat.dateTimeFormat.parse(to);
	
		estTime = (int)(dateTo.getTime() - dateFrom.getTime()) / (60*1000);
//		setEstimatedTime(estTime);
		setDuration(estTime);
		
//		setName(name);
		setDescription(descript);
//		setDate(dateFrom);
		setDateTo(dateTo);
//		setMonths(month);
//		setDays(day);
//		sumEstimatedTime(estTime);
		
		//isDone(done);

	}
	

//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
	
	@Override
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}	
	
//	public void setDate(Date from) {
//		this.day = from.getDate();
//		this.month = from.getMonth();
//	}
	
//	public String getDate() {
//		return getDays()+" "+getMonths();
//	}

	public void setEstimatedTime(int estTime) {
		this.estTime = estTime;
	}
	
	public int getEstimatedTime() {
		return estTime;
	}
	
	public boolean isDone(boolean done) {
		return (done==true)?true:false;
	}

	public void setDuration(int estTime) {
		this.estTime = estTime;
	}
	
	@Override
	public int getDuration() {
		return estTime;
	}

	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return dateFrom;
	}
	
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	
	public Date getDateTo() {
		return dateTo;
	}
	
	@Override
	public String toString() {
		return "Event, "+getDescription()+", "+DateAndTimeFormat.dateTimeFormat.format(getDate())+", "+DateAndTimeFormat.dateTimeFormat.format(getDateTo());
	}

	
//	public void sumEstimatedTime(int estTime) {
//		sumEstTime = sumEstTime+estTime;
//	}
//	
//	public int getSumEstimatedTime() {
//		return this.sumEstTime;
//	}

}
