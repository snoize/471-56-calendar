package model;

import java.util.Date;

abstract class AbstractTimeDuration implements TimeDuration {
	
	private String description;
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String desc) {
		this.description = desc;
	}
	
	@Override
	public int compareTo(TimeDuration o) {
		Date other = o.getDate();
		if(this.getDate().before(other)) {
			return -1;
		}
		if(this.getDate().after(other)) {
			return 1;
		}
		return 0;
	}
	
	
//
//	@Override
//	public Date getDate() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public int getDuration() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public String getType() {
//		// TODO Auto-generated method stub
//		return null;
//	}

	

}
