package model;


import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import controlview.DateAndTimeFormat;

public class Task extends AbstractTimeDuration {

	private String description;
//	private String dueDate;
//	private boolean done;
	private int estTime;
//	private int sumEstTime;
//	private Date from;
//	private Date to;
	
	private String type = "Task"; 
	
//	private int day;
//	private int month;
	
	private String days = "";
	private String months = "";
	
	Account acc = new Account();
	Date date;
	
	public Task() {
		
	}
	
	public Task(String descript, String dueDate, int timeDur/*, boolean done*/) throws ParseException {

		
		date = DateAndTimeFormat.dateFormat.parse(dueDate);

		setDate(date);
		setDuration(timeDur);
		setType(type);
//		setEstimatedTime(estTime);
//		setName(name);
		setDescription(descript);
		setDate(date);
		
//		setMonths(month);
//		setDays(day);
//		isDone(done);
//		sumEstimatedTime(estTime);

	}

	public void setDate(Date date) {
		this.date = date;
 	}
	
//	public void setDate(Date from) {
//		this.day = from.getDate();
//		this.month = from.getMonth();
//	}
	
//	public Date getDate() {
//		return dueDate;
//	}


	public String getDays() {
		return days;
	}
	
	public void setDays(int day) {
		this.days = String.valueOf(day); 
	}

	public void setEstimatedTime(int estTime) {
		this.estTime = estTime;
	}
	
	public int getEstimatedTime() {
		return estTime;
	}
	
	
	@Override
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}	
	
	
//	public String isDone(boolean done) {
//		return (done==true)?"Yes":"No";
//	}
	
//	public boolean notDone() {
//		return false;
//	}
	
	public boolean isDone(boolean done) {
		return (done==true)?true:false;
	}

	public void setDuration(int estTime) {
		this.estTime = estTime;
	}
	
	@Override
	public int getDuration() {
		return estTime;
	}

	@Override
	public Date getDate() {

		return date;
	}
	
	@Override
	public String toString() {
		return "Task, "+getDescription()+", "+DateAndTimeFormat.dateFormat.format(getDate())+", "+getDuration();
	}


	
	
}
