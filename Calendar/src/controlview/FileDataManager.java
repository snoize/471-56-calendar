package controlview;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;

import model.Account;

public class FileDataManager {
	
	public Account acc;
	String[] info;
	String data;
	private BufferedReader br;
	
	public FileDataManager(Account acc) {
		this.acc = acc;
	}
	
	public void readData() throws IOException, ParseException {
	
		br = new BufferedReader(new FileReader("src/textfile.txt"));
		while((data = br.readLine()) != null) {
			data.trim();
			info = data.split(",");

			if(info[0].equals("Task")) {
				model.Task t = new model.Task(info[1].trim(), info[2].trim(), Integer.parseInt(info[3].trim()));
				acc.list.add(t);
			}
			else if(info[0].equals("Event")) {
				model.Event e = new model.Event(info[1].trim(), info[2].trim(), info[3].trim());
				acc.list.add(e);
				int len = info[1].length();
				
			}
		}		
		
	}
	
	public void writeData() throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter("src/textfile.txt"));
		writer.flush();
		for(int z=0; z<acc.list.size(); z++){
			if(acc.list.get(z).getType().equals("Event")) {
				writer.write(acc.list.get(z).toString()+"\n");
			}
			else if(acc.list.get(z).getType().equals("Task")) {
				writer.write(acc.list.get(z).toString()+"\n");
			}
		}
		writer.close();
		
	}

}
