package controlview;

import java.awt.Desktop.Action;
import java.awt.Event;
import java.awt.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Scanner;

import javax.sound.midi.MidiDevice.Info;
import javax.xml.bind.ParseConversionEvent;

import model.Account;
import model.Task;


public class CalendarConsole {
	
	Account acc = new Account();
	FileDataManager rw = new FileDataManager(acc);
	String[] info;
	String data;
//	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
	boolean condition = true;
	
	public CalendarConsole() throws IOException, ParseException {
		
		
		rw.readData();
		System.out.println("Welcome to Snoize Calendar");
		while(condition) {

			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("\nChoose action (c = create, d = display, q = quit) : ");
			String action = in.readLine().trim();
			if(action.equals("c")) {
				System.out.println("Create task or event (t, e) : ");
				action = in.readLine().trim();
				if(action.equals("t")) {
					System.out.println("Add task (desc, due date, est. time duration) : ");
					data = in.readLine().trim();
					info = data.split(",");
					
					
					model.Task t = new model.Task(info[0], info[1], Integer.parseInt(info[2].trim()));
					acc.list.add(t);
					Collections.sort(acc.list);
				}
				else if(action.equals("e")) {
					System.out.println("Add event (desc, from, to) : ");
					String data = in.readLine().trim();
					info = data.split(",");

					model.Event e = new model.Event(info[0], info[1], info[2]);
					
					acc.list.add(e);
					Collections.sort(acc.list);
				}

				
				System.out.println("Done adding!");
			}
			else if(action.equals("d")) {
//				acc.sort();
				display();
				
				printTotalEstimatedTime(acc.getTotalTime());
			}
			else if(action.equals("q")) {
				//Save and Quit program
				rw.writeData();
				condition = false;
			}
		}
		
			
	}
	
	public void welcomeMsg(String name) {
		System.out.println("Welcome to "+name+" Calendar\n");
	}
	
	public void display() {
		Date today = new Date();
		System.out.printf("%-40s", "Item");
		System.out.println("Type\t\tEst. Time\tDate\t\tDone");
		System.out.println("=====\t\t\t\t\t====\t\t=========\t====\t\t====");
		for(int j=0; j<acc.list.size(); j++) {
			System.out.printf("%-40s", acc.list.get(j).getDescription());
			System.out.print(acc.list.get(j).getType()+"\t\t"+acc.list.get(j).getDuration()+"\t\t");
			//System.out.printf("%02d/%02d/%d", acc.list.get(j).getDate().getDate(), acc.list.get(j).getDate().getMonth(), acc.list.get(j).getDate().getYear()+1900);
			System.out.print(DateAndTimeFormat.dateFormat.format(acc.list.get(j).getDate()));
			System.out.print("\t");
			if (acc.list.get(j).getType()=="Task") {
				if(today.after(acc.list.get(j).getDate())) {
					System.out.print("Yes");
				}
				else {
					System.out.print("No");
				}
			}
			else {
				System.out.print("-");
			}
			
			System.out.println();
		}
		
	}
	
	public void printDetails(Object Obj, String descript, String type, int estTime, String date, boolean done) {
		
		System.out.printf("%-40s", descript);
		System.out.print(""/*+descript+"\t\t\t"*/+type+"\t\t"+"   "+estTime+"\t\t"+date+"\t\t"+" "/*+done*/);
		if (Obj instanceof model.Task) {
			if(done==true) {
				System.out.print("Yes");
			}
			else {
				System.out.print("No");
			}
		}
		else {
			System.out.print("-");
		}
		System.out.println("");
		
	}
	
	public void printTotalEstimatedTime(int estTime) {
		System.out.println("\nTotal estimated time: "+estTime+" minutes");
	}


}
