package controlview;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import model.Account;
import model.Event;
import model.Task;

import org.junit.Before;
import org.junit.Test;

public class CalendarTest {

	model.Account snoizeCalendar;
	private String descript;
	private String from;
	private String to;
	private String dueDate;
	private int timeDur;
	private Event e;
	private Task t;
	
	
	@Before
	public void testMain() throws ParseException {
		
		snoizeCalendar = new model.Account("Snoize");
	
		descript = "Study 01418471 Lab";
		from = "27/06/13 15:30";
		to = "27/06/13 17:30";
		e = new Event(descript, from, to);
		snoizeCalendar.add(e);
		
		descript = "Meeting with KimSungkyu";
		from = "28/09/13 18:00";
		to = "28/09/13 22:00";
		e = new Event(descript, from, to);
		snoizeCalendar.add(e);
		
		descript = "Read a book";
		dueDate = "21/07/13";
		timeDur = 180;
		t = new Task(descript, dueDate, timeDur);
		snoizeCalendar.add(t);
		
		descript = "Meeting with BTOB";
		from = "06/07/13 16:00";
		to = "06/07/13 17:00";
		e = new Event(descript, from, to);
		snoizeCalendar.add(e);
		
		descript = "Write essay book";
		dueDate = "27/07/13";
		timeDur = 240;
		t = new Task(descript, dueDate, timeDur);
		snoizeCalendar.add(t);

	}
	
	@Test
	public void testTotalTime() {
		assertEquals(840, snoizeCalendar.getTotalTime());
	}
	
	@Test
	public void testSort() {
//		snoizeCalendar.sort();
		Collections.sort(snoizeCalendar.list);
		assertEquals("Meeting with KimSungkyu", snoizeCalendar.list.get(4).getDescription());
		assertEquals("Write essay book", snoizeCalendar.list.get(3).getDescription());
		assertEquals("Read a book", snoizeCalendar.list.get(2).getDescription());
		assertEquals("Meeting with BTOB", snoizeCalendar.list.get(1).getDescription());
		assertEquals("Study 01418471 Lab", snoizeCalendar.list.get(0).getDescription());

	}

}
