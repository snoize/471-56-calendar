package controlview;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import model.Account;

public class CalendarGUI extends JFrame {

	Account acc = new Account();
	FileDataManager rw = new FileDataManager(acc);
	
	model.Task t;
	model.Event e;
	
	JTextArea result;
	JTextField desc;
	JTextField from;
	JTextField to;
	
	JLabel lbl1, lbl2, lbl3, lbl4;
	
	
	JRadioButton event;
	JRadioButton task;
	
	JPanel createPanel;
	JPanel displayPanel;
	JPanel resultPanel;
	JPanel inLeft;
	JPanel inRight;
	
	
	Box typeBox;
	ButtonGroup typeGroup;
	JButton addBtn;
	JButton sortByDate;
	
	ActionListener listener;
	WindowListener winListener;
	
	int len;
	int estTime;
	String descrip;
	String done;
	
	public CalendarGUI() throws IOException, ParseException {
		
		
//		super(str);
//		setBackground(Color.ORANGE);
		rw.readData();
				
		
		JFrame frame = new JFrame();
		frame.setTitle("Welcome to Snoize"+"\'s "+"Calendar");
		frame.setSize(800, 500);
		frame.setResizable(true);
		frame.setLocationRelativeTo(null);
//		addComponentsToPane(frame.getContentPane());
		frame.getContentPane();
		
		
		Container pane = getContentPane();
		pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
//		JButton button;
//		pane.setLayout(new GridBagLayout());
		pane.setLayout(new GridLayout(1, 2));
		GridBagConstraints c = new GridBagConstraints();
		
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new GridLayout(2, 1));

		
		displayPanel = new JPanel();
		Border etched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
//		displayPanel.setLayout(new GridBagLayout());
		TitledBorder displayBorder = BorderFactory.createTitledBorder(etched, "Display");
		displayPanel.setBorder(displayBorder);
		
		desc = new JTextField(10);
		from = new JTextField(10); 
		to = new JTextField(10);
		
		lbl1 = new JLabel("Type: ");
		lbl2 = new JLabel("Description: ");
		lbl3 = new JLabel("From: ");
		lbl4 = new JLabel("To: ");
		
		createPanel = new JPanel();
//		createPanel.setLayout(new GridLayout(5,1));
		createPanel.setLayout(new GridBagLayout());
		TitledBorder createBorder = BorderFactory.createTitledBorder(etched, "Create");
		createPanel.setBorder(createBorder);
		
		inLeft = new JPanel();
		inLeft.setLayout(new GridLayout(5,1));
		
	    inRight = new JPanel();
	    inRight.setLayout(new GridLayout(5,1));
		
		event = new JRadioButton("Event", true);
		event.setActionCommand("Event");
		task = new JRadioButton("Task", false);
//		task.setActionCommand("Task");
//		event.setSelected(true);
		

		
		typeBox = Box.createHorizontalBox();
		typeGroup = new ButtonGroup();
		typeGroup.add(event);
		typeGroup.add(task);
		typeBox.add(event);
		typeBox.add(task);
		addBtn = new JButton("Add");
		
		
		 frame.addWindowListener(new WindowListener() {

		        @Override
		        public void windowClosing(WindowEvent e) {
		            try {
						rw.writeData();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		        }

		        @Override 
		        public void windowOpened(WindowEvent e) {}

		        @Override 
		        public void windowClosed(WindowEvent e) {}

		        @Override 
		        public void windowIconified(WindowEvent e) {}

		        @Override 
		        public void windowDeiconified(WindowEvent e) {}

		        @Override 
		        public void windowActivated(WindowEvent e) {}

		        @Override 
		        public void windowDeactivated(WindowEvent e) {}

		    });
		
	    class Listener implements ActionListener {

			public void actionPerformed(ActionEvent ev) {

				if(task.isSelected()) {
					lbl3.setText("Due Date:");
					lbl4.setText("Estimated Duration:");
				}
				if(event.isSelected()) {
					lbl3.setText("From:");
					lbl4.setText("To:");
				}
				if(task.isSelected() && ev.getSource()==addBtn) {
					System.out.println("add Button");

					try {
						t = new model.Task(desc.getText(), from.getText(), Integer.parseInt(to.getText()));
					} catch (NumberFormatException | ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					acc.list.add(t);
					Collections.sort(acc.list);
					result.setText("Done adding!");
					desc.setText(null);
					from.setText(null);
					to.setText(null);
				}
				
				if(event.isSelected() && ev.getSource()==addBtn) {
					System.out.println("add Button");
					try {
						e = new model.Event(desc.getText(), from.getText(), to.getText());
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					acc.list.add(e);
					Collections.sort(acc.list);
					result.setText("Done adding!");
					desc.setText(null);
					from.setText(null);
					to.setText(null);
				}

				if(ev.getSource() == sortByDate) {
					Collections.sort(acc.list);
					
					display();

				
				}
			}
		}
	    listener = new Listener();
//	    winListener = new Window();
		
		event.addActionListener(listener);
		task.addActionListener(listener);
		addBtn.addActionListener(listener);
//		JButton clearBtn = new JButton("Clear");

		
		
		addItem(inLeft, lbl1, 0, 0, 1, 1, GridBagConstraints.EAST);
		addItem(inRight, typeBox, 0, 1, 1, 1, GridBagConstraints.WEST);
		
		addItem(inLeft, lbl2, 1, 0, 1, 1, GridBagConstraints.EAST);
	    addItem(inRight, desc, 1, 1, 1, 1, GridBagConstraints.WEST);
		
	    addItem(inLeft, lbl3, 2, 0, 1, 1, GridBagConstraints.EAST);
	    addItem(inRight, from, 2, 1, 1, 1, GridBagConstraints.WEST);
	    
	    addItem(inLeft, lbl4, 3, 0, 1, 1, GridBagConstraints.EAST);
	    addItem(inRight, to, 3, 1, 1, 1, GridBagConstraints.WEST);
	    
	    addItem(inLeft, addBtn, 4, 0, 1, 1, GridBagConstraints.EAST);
//	    addItem(inRight, clearBtn, 4, 1, 1, 1, GridBagConstraints.WEST);
	    
	    createPanel.add(inLeft);
		createPanel.add(inRight);
	    
	    
//	    addItem(createPanel, task, 1, 0, 1, 1, GridBagConstraints.WEST);
//	    addItem(createPanel, event, 1, 1, 1, 1, GridBagConstraints.WEST);

//	    addItem(createPanel, to, 3, 1, 1, 1, GridBagConstraints.WEST);
//	    addItem(createPanel, clearBtn, 1, 4, 1, 1, GridBagConstraints.WEST);
	    

		
		sortByDate = new JButton("Sort by date");
		sortByDate.addActionListener(listener);
		addItem(displayPanel, sortByDate, 0, 5, 1, 1, GridBagConstraints.NORTH);
		

	    leftPanel.add(createPanel);
	    leftPanel.add(displayPanel);
	    
	    
	    JPanel rightPanel = new JPanel();
	    rightPanel.setLayout(new GridLayout(2,1));
	    
		resultPanel = new JPanel();
		resultPanel.setLayout(new BorderLayout());
		
		
		result = new JTextArea();
		result.setEditable(false);
//		addItem(resultPanel, result, 0, 0, 10, 20, GridBagConstraints.CENTER);
		etched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		TitledBorder resultBorder = BorderFactory.createTitledBorder(etched, "Result");
		JScrollPane bar = new JScrollPane(result);
		resultPanel.add(bar);
		bar.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		

	    
	    JLabel resultLb = new JLabel("Result");
	    resultPanel.add(resultLb, BorderLayout.NORTH);
	    pane.add(resultPanel);
	    pane.add(leftPanel);
	    
	    frame.add(pane);
	    frame.setVisible(true);
	    display();

	}

	private void addItem(JPanel p, JComponent c, int x, int y, int width, int height, int align) {
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = x;
		gc.gridy = y;
		gc.gridwidth = width;
		gc.gridheight = height;
//		gc.weightx = 100.0;
//		gc.weighty = 100.0;
		gc.insets = new Insets(5, 5, 5, 5);
		gc.anchor = align;
//		gc.fill = GridBagConstraints.NONE;
		p.add(c, gc);
	}

	
	

	public void display() {
		
		result.setText("Item\t\tType\tEst. Time\tDate\tDone\n=====\t\t====\t=========\t====\t====\n");
		//result = new JTextArea();
		Date today = new Date();
		//System.out.printf("%-40s", "Item");
		//System.out.println("Type\t\tEst. Time\tDate\t\tDone");
		//System.out.println("=====\t\t\t\t\t====\t\t=========\t====\t\t====");
		for(int j=0; j<acc.list.size(); j++) {
			
			len = acc.list.get(j).getDescription().length();
			if(len<30) {
				descrip = acc.list.get(j).getDescription();
				for(int x=len; x<30; x++) {
					descrip = descrip.concat(" ");
				}
			}
			
			if (acc.list.get(j).getType()=="Task") {
				if(today.after(acc.list.get(j).getDate())) {
					done = "Yes";
				}
				else {
					done = "No";
				}
			}
			else {
				done = "-";
			}
			result.append(descrip+"\t"+acc.list.get(j).getType()+"\t"+acc.list.get(j).getDuration()+"\t"+DateAndTimeFormat.dateFormat.format(acc.list.get(j).getDate())+"\t"+done+"\t\n");
		}
	estTime = acc.getTotalTime();
	result.append("\nTotal estimated time: "+estTime+" minutes\n");	
	}
	
}
